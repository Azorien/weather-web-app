function WeatherFetcher(key){
    this.get5DayWeather = function(city, country, callback){
        $.ajax({
            url: "https://api.openweathermap.org/data/2.5/forecast",
            data:{
                q: city + "," + country,
                appid: key
            },
            success: function(data){
                callback(data);
            }
        });
    }
}