const weatherFetcher = new WeatherFetcher("42b1d0158d7a7fe1e3cd7707f6ca9c80");
const locationFinder = new LocationFinder("0bd53819-43e7-4c62-88bc-8a4380001f82");

// const days = ["Mon", "Tue"];


var app = new Vue({
    el: '#app',
    methods: {
        setActive(index){
            this.activeIndex = index
        },
        updateWeather: function(city, country){
            weatherFetcher.get5DayWeather(city, country, function(data) {
                console.log(data);
                this.weatherData = data;
            }.bind(this));
        },
        makeTimeText: function(time){
            var date = new Date(time*1000);
            return "" + date.getHours() + ":" + ((date.getMinutes()<10) ? "0" : "") + date.getMinutes();
        },
        makeDayOfWeekText: function(time){
            var date = new Date(time*1000);
            return ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"][date.getDay()];
        },
        kelvinToCelsius: function(kelvin){
            return Math.round(kelvin - 273.15);
        },
        isSelected: function(index){
            return this.activeIndex == index;
        }
    },
    created: function(){
        console.log("setup")
        locationFinder.findLocation(function(city, country){
            console.log(city, country)
            if(city){
                this.updateWeather(city, country)
            }
        }.bind(this))
    },
    data:{
        activeIndex: 0,
        weatherData: {}
    },
})