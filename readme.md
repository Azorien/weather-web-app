# Weather Web App

### Building / Running
The project can be deployed out of the box, does not require building
To view locally - open index.html file
Currently there are no automated tests

### Creation of the App and Tradeoffs
Upon receiving initial requirements I noticed no golbal dependencies should be used which influenced my choices. 
I decided to ensure everything works on a system that has nothing more than a browser installed. For this I work purely with VueJS from CDN and not use any package managers.
This takes away some opportunities such as using VueJS single file components, but this is not a big deal for a small to medium size project.

The web page is mostly designed based on structure of OpenWeatherAPI response - giving user a list of weather forecasts for next 5 days (which are returned for each 3 hours). I also decided to add a top section that could contain more detailed information than smaller list elements

### Wishlist
If I had more time I would add:

* Improved graphic design

* Error handling

* Location change

* improve user experience (I would want to replace horizontal scrolling with better solution)