function LocationFinder(key){
    this.findLocation = function(callback){
        $.ajax({
            url: "https://ipfind.co/me?auth=" + key,
            success: function(data){
                callback(data.city, data.country_code);
            }
        })
        
    }
}